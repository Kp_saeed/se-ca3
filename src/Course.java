import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

public class Course {

    private String name;
    private int courseCode;
    private int units;
    private LocalDateTime startExam;
    private LocalDateTime endExam;
    private ArrayList<Course> prerequisitesList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(String firstDay) {
        this.firstDay = firstDay;
    }

    public String getSecondDay() {
        return secondDay;
    }

    public void setSecondDay(String secondDay) {
        this.secondDay = secondDay;
    }

    public String getExamDay() {
        return examDay;
    }

    public void setExamDay(String examDay) {
        this.examDay = examDay;
    }

    public int getExamTime() {
        return examTime;
    }

    public void setExamTime(int examTime) {
        this.examTime = examTime;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public ArrayList<String> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(ArrayList<String> studentsList) {
        this.studentsList = studentsList;
    }

    public ArrayList<String> getSudentWaitingList() {
        return sudentWaitingList;
    }

    public void setSudentWaitingList(ArrayList<String> sudentWaitingList) {
        this.sudentWaitingList = sudentWaitingList;
    }

    public ArrayList<String> getPrerequisitesList() {
        return prerequisitesList;
    }

    public void setPrerequisitesList(ArrayList<String> prerequisitesList) {
        this.prerequisitesList = prerequisitesList;
    }

}