import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

public class Offering extends Course{

    private int classCode;
    private Instructor instructor;
    private LocalTime startTime;
    private LocalTime endTime;
    private ArrayList<Days> classDays;
    private int capacity;
    private int waitingListCapacity;
    private ArrayList<Student> studentsSignedUpList;
    private ArrayList<Student> studentWaitingList;
    private boolean canBeChosen;


    public enum Days {
        saturday,
        sunday,
        monday,
        tuesday,
        wednesday,
        thursday,
        friday
    }

}
