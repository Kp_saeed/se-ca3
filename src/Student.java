import java.util.ArrayList;
import java.util.HashMap;

public class Student {

    private String firstName;
    private String lastName;
    private int phoneNumber;
    private int studentNumber;
    private String email;
    private int gpa;
    private int termUnits;
    private int totalUnits;
    private String field;
    private String faculty;


    private HashMap<Integer, HashMap<Offering, Double>> educationalRecord = new HashMap<>();
    private ArrayList<AddedOffering> offerings;



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<String> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<String> courses) {
        this.courses = courses;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
       this.email = email;
    }

    public int getGpa() {
        return gpa;
    }

    public void setGpa(int gpa) {
        this.gpa = gpa;
    }

}