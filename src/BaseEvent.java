import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.security.MessageDigest;

public class BaseEvent{
    private String eventID; // this could be the hash of request body
    private LocalTime eventTime;
    private String eventType; // could be endpoint eg www.ems.com/course/enroll
    private String eventIssuer;
    private String eventProperties; // properties of event in json string could be request body
    private boolean isProcessed; 
    private boolean isValid;

    public void setEventID() {
        md5 = MessageDigest.getInstance("MD5");
        this.eventID = md5.digest(this.eventProperties);
    }

    public void setEventProperties(String eventProperties) {
        this.eventProperties = eventProperties;
    }
}
